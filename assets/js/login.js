let loginForm = document.querySelector("#loginUser")

loginForm.addEventListener("submit", (e) => {
    e.preventDefault()

    let email = document.querySelector("#userEmail").value 
    let password = document.querySelector("#password").value 

    //lets create first a validation that will allow us to determine if the input fields are not empty. 
    if(email == "" || password == "") {
        alert("Please input email and/or password!")
    } else {
        fetch('https://prince-cs2.herokuapp.com/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password,

            })
        })
        .then(res =>{
            return res.json()
        })
        .then(data => {
            //to check if the data has been caught, lets display it first in the console.
            console.log(data)
            //upon successful authentication it will return a JWT
            //lets create a control structure to determine if the JWT has been generated together with the user credentials in a object format.
            if(data.access){
                //we are going to store now the JWT inside our localStorage. 
                // local storage readings =>  https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage
                localStorage.setItem('token', data.access)
                fetch('https://prince-cs2.herokuapp.com/api/users/details', {
                    headers: {
                        Authorization: `Bearer ${data.access}`
                    }
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    localStorage.setItem('id', data._id) 
                    localStorage.setItem('isAdmin', data.isAdmin)
                    localStorage.setItem('firstName', data.firstName)

                    if(data.isAdmin == false){
                        window.location.replace("./profile.html")
                    }else {
                        window.location.replace("./courses.html")
                    }
                    
                })
                //once the id is authenticated successfully using the access token then it should redirect the user to the user's profile page.
                
            } else{
                //create an else branch that will run if an access key is  not found.
                alert("Something went wrong, check your credentials!")
            }

        })
    }
})