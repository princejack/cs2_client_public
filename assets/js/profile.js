
let myJumbotron = document.querySelector("#myJumbotron")
let amIadmin = localStorage.getItem('isAdmin')
console.log(amIadmin)


let token = localStorage.getItem('token');
console.log(token);


if(!token || token === null){
	window.location.href = "./login.html"
} else{
	fetch('https://prince-cs2.herokuapp.com/api/users/details', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())

	.then(data => {

		console.log(data)
			
		const enrollmentData = data.enrollments.map(classData => {
			console.log(classData)
			return(
				`
				<tr>
				   <td>${classData.courseId}</td> 
				   <td>${classData.enrolledOn}</td> 
				   <td>${classData.status}</td> 
				</tr> 
				`
				// console.log(classData.join(', '))
			)
		}).join(' ')


		myJumbotron.innerHTML = 
		 `<div class="com-md-12">
				<section class="jumbotron my-5">
					<h3 class="text-center">First Name: ${data.firstName}</h3>
					<h3 class="text-center">Last Name: ${data.lastName}</h3>
					<h3 class="text-center">Email: ${data.email}</h3>
					<h3 class="text-center">Class History</h3>
					<table class="table">
						<thead>
							<tr>
								<th> Course ID </th>
								<th> Enrolled On </th>
								<th> Status </th>
							</tr>
							<tbody>
								${enrollmentData}
							</tbody>
						</thead>
					</table>
				</section>
			</div>
		`
// ${enrollmentData} - put in tbody


	})
}	




let logoutButton = document.getElementById("logoutButton");

logoutButton.addEventListener("click", function(){
	localStorage.clear();
	window.location.replace('./logout.html');
})