let token = localStorage.getItem('token');
let isAdminStorage = localStorage.getItem('isAdmin')
let firstNameStorage = localStorage.getItem('firstName')
let showCourses = document.querySelector("#showCourses")
let adminButtons = document.querySelector("#adminButton")
let userButton = document.querySelector("#userButton")
let welcome = document.querySelector("#welcome")
let navitem = document.querySelector(".navbar-nav")
console.log(token)




let _activeCards = [];
let cardsId = [];



function pushCards(courses){
    for(i=0;i<courses.length;i++){

        let showCourseDetails = 
        `
                                
                       
       <div class="col-md-4 my-3">
       <a href="./course.html?id=${(courses[i]._id)}" style="text-decoration:none" class="text-dark">
                <div class="card">
                    <div class="card-header" id="adminButtons">
                        
                         <img src="https://miro.medium.com/max/671/1*3VFoTXnAp2EUxiWGLZAGWw.png" class="card-img-top" alt="...">

                    </div>
                    <div class="card-body">
                        <h2 class="card-title"> ${courses[i].name}</h2>
                        <p class="card-title"> ${courses[i].description}</p>
                        
                    </div>
                    <div class="card-footer" id="userButton">
                    
             
        
        `
 
        _activeCards.push(showCourseDetails)

    }
}


function populateCards(courses){
    let course = document.querySelector(".card-deck")
    if(isAdminStorage === 'true') { //ADMIN ACCESS

        let textIsActive = ""

        for(i = 0; i < _activeCards.length; i++)  {
            if(courses[i].isActive){
                textActive = "ACTIVE";
                
                
                
            }else{
                textActive = "INACTIVE";
            }
            _activeCards[i] += `<h5 id="courseStatus">${textActive}</h5> 
                                </div></div></div> </a>
                                `;
            course.innerHTML += _activeCards[i];
        }




        // 
        welcome.innerHTML = "Welcome back Admin!"
         navitem.innerHTML = 
            `
                <li class="nav-item active">
                    <a class="nav-link text-light" href="../index.html">Home<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-light" href="./addCourse.html">Add Course</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-light" href="#" onclick="logout()">Log out</a>
                </li>
           

            `
        //

    }
    if(isAdminStorage === 'false'){ //STUDENT ACCESS
        for(let i=0; i< _activeCards.length; i++){
            _activeCards[i] += `<h5 id="courseStatus">  &#8369;${courses[i].price}</h5> 
                                </div>`;
            course.innerHTML += _activeCards[i];

        }
        welcome.innerHTML = `Welcome Back to Code Verse, ${firstNameStorage}!`
                 navitem.innerHTML = 
            `
                <li class="nav-item active">
                    <a class="nav-link text-light" href="../index.html">Home<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-light" href="./profile.html">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-light" href="#" onclick="logout()">Log out</a>
                </li>
           

            `


    }   

    for(let i=0; i< _activeCards.length; i++) {
        let goCourse = document.getElementById(`${courses[i]._id}`)
        goCourse = addEventListener("click", () => {

            sessionStorage.setItem("courseId", courses[i]._id)
            console.log(courses[i]._id)

        })
    }
}





showCourses.innerHTML = 
`
<section class="container-fluid padding cards-container my-5">
    <div class="row padding text-dark">
        <div class="card-deck parent mb-5">
            
        </div>
    </div>
</section>
`


if(token === null){ //if token is NOT accessed
    alert("Make sure to login first")
    window.location.replace("./login.html")
}else{ //if token is accessed
    if(isAdminStorage === 'true'){
        fetch('https://prince-cs2.herokuapp.com/api/courses/admin', {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json'
            }
        })
        .then(res => {
            return res.json()
        })
        .then(courses => {
            pushCards(courses)
            populateCards(courses)
        })



    }
    else{
        fetch('https://prince-cs2.herokuapp.com/api/courses/user', {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json'
            }
        })
        .then(res =>{
            return res.json()
        })
        .then(courses => {
            pushCards(courses)
            populateCards(courses)
            console.log(courses)
        })

    }

}

function logout(){
    localStorage.clear();
    sessionStorage.clear();
    window.location.replace('./logout.html');
}