
let userId = localStorage.getItem("id")
let ifActive = sessionStorage.getItem("ifActive")
let isAdminStorage = localStorage.getItem('isAdmin')
let courseName = document.querySelector("#courseName2")
let coursePrice = document.querySelector("#coursePrice")
let _courseId = sessionStorage.getItem("courseId")
let userFirstName = localStorage.getItem("firstName")


let parameter = window.location.search;
const urlId = new URLSearchParams(parameter)
let courseId = urlId.get('id');








let token = localStorage.getItem('token');

console.log(token);



if(_courseId !== null){
	if(isAdminStorage === 'true'){
		fetch(`https://prince-cs2.herokuapp.com/api/courses/${courseId}`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		})
		.then(res => {
			return res.json()
		})
		.then(course => {
			courseName.innerHTML = course.name
			courseDescription.innerHTML = course.description
			coursePrice.innerHTML = course.price

			let isChecked = ""
			let main = document.querySelector("#main")

			let textActive = ""
			if(course.isActive) isChecked = "checked"

				



			
			


			let ifAdmin = 
				`
					<div class="col-md-10 offset-1">
						<section class="jumbotron">
							<h2 class="text-dark">Enrolled Students:</h2>
							<table class="table table-light">
								<thead>
									<tr>
										<th class="text-center">First Name</th>
										<th class="text-center">Last Name</th>
										<th class="text-center">Email</th>
									</tr>
									<tr>
										<th class="text-center" id="studentFname"></th>
										<th class="text-center" id="studentLname"></th>
										<th class="text-center" id="studentEmail"></th>
									</tr>
								</thead>
							</table>
						</section>
						<section class="text-center pb-5">
						<h2 class="text-dark" id="courseIsActive">Course Status:  ${course.isActive}</h2>
						<label class="switch">
							<input type="checkbox" id="statusBtn" ${isChecked}>
							<span class="slider round"></span>
						</label>
						</section>
					</div>
				

				`
				// change status to active or inactive ----------------
					
				main.innerHTML = ifAdmin;


				let statusBtn = document.querySelector("#statusBtn")
				let courseIsActive = document.getElementById("courseIsActive")

				


				if(statusBtn.checked == true){
					let textisActive = "Active"
				}else{
					textisActive ="Inactive"
				}

				statusBtn.addEventListener("click", () => {
					if(statusBtn.checked === true){
						fetch(`https://prince-cs2.herokuapp.com/api/courses/${courseId}`, {
							method: "PUT",
							headers: {
								"Content-Type": "application/json",
								"Authorization": `Bearer ${token}`
							},
							body: JSON.stringify({
								isActive: true
							})
						})
						.then(res =>{
							return res.json()
						})
						courseIsActive.innerHTML = "Course Status: ACTIVE"

					} else{
						fetch(`https://prince-cs2.herokuapp.com/api/courses/${courseId}`, {
							method: "DELETE",
							headers: {
								'Content-Type': 'application/json',
								'Authorization': `Bearer ${token}`
							}
						})
						.then(res => {
							return res.json()
						})
						courseIsActive.innerHTML = "Course Status: INACTIVE"
					}
				})
		})
	}else{//student access

		fetch(`https://prince-cs2.herokuapp.com/api/courses/${courseId}`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",

			}

		})
		.then(res => {
			return res.json()
		})
		.then(course => {
			
			courseName.innerHTML = course.name
			courseDescription.innerHTML = course.description
			coursePrice.innerHTML = course.price




			 
		})

		let ifStudent = 
				`
					<div class="col-md-4 offset-4">
					<a class="btn text-white" id="enrollBtn" onclick="enrollThisCourse()"><span>Enroll Now</span></a>
						
						<section class="text-center pb-5">

						</section>
					</div>
				

				`
				main.innerHTML = ifStudent

	}
}



function enrollThisCourse(){	

				fetch(`https://prince-cs2.herokuapp.com/api/users/enroll`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					},
					body: JSON.stringify({
						courseId: courseId
					})
				})
				.then(res => res.json())

				.then(data => {
					alert("Enrolled Successfully")
				})


		}


	
	






function goToCourses(){
	sessionStorage.clear()
	window.location.replace('./courses.html')
}

