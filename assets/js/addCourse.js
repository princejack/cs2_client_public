let createCourse = document.querySelector("#createCourse")

let token = localStorage.getItem('token')
console.log(token)


	createCourse.addEventListener("submit", (e) => {
		e.preventDefault()
		let name = document.querySelector("#courseName").value
		let price = document.querySelector("#coursePrice").value
		let description = document.querySelector("#courseDescription").value
		let checkBox = document.getElementById("isActive")


		




		fetch('https://prince-cs2.herokuapp.com/api/courses/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				price: price,
				description: description,
				isActive: checkBox
			})
		})


		.then(res => {
			return res.json()
		})

		.then(data => {
			console.log(data)

			if(data == true){

				alert("Successfully added a course")
				window.location.replace("./courses.html")
			}else{
				alert("Something went wrong!")
			}
		})



	})