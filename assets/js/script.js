let logoutButton = document.getElementById("logoutButton");
let showAllCourses = document.querySelector("#showAllCourses")
let isAdminStorage = localStorage.getItem('isAdmin')

let _activeCards = [];
let cardsId = [];



function pushCards(courses){
    for(i=0;i<courses.length;i++){

        let showCourseDetails = 
        `

          <div class="col-md-4 my-3">
         
                <div class="card">
                    <div class="card-header" id="adminButtons">
                        <img src="https://miro.medium.com/max/671/1*3VFoTXnAp2EUxiWGLZAGWw.png" class="card-img-top" alt="...">
                    </div>
                    <div class="card-body">
                        <h2 class="card-title"> ${courses[i].name}</h2>
                        <p class="card-title"> ${courses[i].description}</p>
                        
                    </div>
                    
                   

            
       
            
        
        `
        _activeCards.push(showCourseDetails)
        // console.log(_activeCards[i])
    }
}


function populateCards(courses){
    let course = document.querySelector(".card-deck")
     //STUDENT ACCESS
        for(let i=0; i< _activeCards.length; i++){
            _activeCards[i] += `<div class="card-footer" id="userButton"><p class="card-text">Price: ${courses[i].price}</p> </div>
                </div>
            </div>`;
            showAllCourses.innerHTML += _activeCards[i];

        }
       
             
      

    for(let i=0; i< _activeCards.length; i++) {
        let goCourse = document.getElementById(`${courses[i]._id}`)
        goCourse = addEventListener("click", () => {

            sessionStorage.setItem("courseId", courses[i]._id)
            console.log(courses[i]._id)

        })
    }
}


//------------------PAGE SHOW COURSES-------


showAllCourses.innerHTML = 
`
<section class="container-fluid padding cards-container my-5">
    <div class="row padding text-dark">
        <div class="card-deck parent mb-5">
            
        </div>
    </div>
</section>
`




        fetch('https://prince-cs2.herokuapp.com/api/courses/user', {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json'
            }
        })
        .then(res =>{
            return res.json()
        })
        .then(courses => {
            pushCards(courses)
            populateCards(courses)
        })
   




//


logoutButton.addEventListener("click", (e) => {
    e.preventDefault()
	localStorage.clear();
	window.location.replace('./pages/logout.html');
})