let token = localStorage.getItem('token');
let adminButtons = document.querySelector("#adminButton")
let userButton = document.querySelector("#userButton")
let welcome = document.querySelector("#welcome")
let navitem = document.querySelector(".navbar-nav")





    // admin/User buttons
if(token){
    fetch(`https://prince-cs2.herokuapp.com/api/users/details`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
     })
     .then(res => res.json())
     .then(data => {
     
    if(data.isAdmin === true){
        welcome.innerHTML = `Welcome Back Admin!`
        navitem.innerHTML = 
        `
			<li class="nav-item active">
				<a class="nav-link text-light" href="../index.html">Home<span class="sr-only">(current)</span></a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-light" href="./addCourse.html">Add Course</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-light" href="#" onclick="logout()">Log out</a>
			</li>
       

        `

        // adminButtons.innerHTML = 
        // `
        //     <div class="col-md-4 offset-md-4">
        //         <div class="card">
        //             <a href="./addCourse.html" class="btn btn-sm btn-dark">
        //             Add a Course
        //             </a>
        //         </div>
        //     </div>
        
        // `
    const get = fetch(`https://prince-cs2.herokuapp.com/api/courses/`, {
        headers: {
        'Authorization': `Bearer ${token}`
        }

    })


        get.then(res => res.json())
        .then(data => {

    let courses;
    for(let i = 0; i< data.length; i++){
        console.log(data[i]._id)
        if(courses){
            courses = `${courses}
            <div class="col-md-4 my-3">
                <div class="card">
                    
                    <div class="card-body">
                        <h2 class="card-title"> ${data[i].name}</h2>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ${data[i].price}</p>
                    </div>
                    <div class="card-footer" id="userButton">
                        
                         
                        <a href="./deleteCourse.html" class="btn btn-dark  onclick">
                                Delete
                        </a> 
                                             
                    </div>
                </div>
            </div>`
        }else{

            courses = `<div class="col-md-4 my-3">
                <div class="card">
                    
                    <div class="card-body">
                        <h2 class="card-title"> ${data[i].name}</h2>
                        <p class="card-title"> ${data[i].description}</p>
                        <p class="card-text">Price: ${data[i].price}</p>
                    </div>
                    <div class="card-footer" id="userButton">
                       
                        <a href="./deleteCourse.html" class="btn btn-dark  onclick">
                                Delete
                        </a> 
                                        
                    </div>
                </div>
            </div>`
        }

        if(courses){
            document.querySelector("#courseContainer").innerHTML = courses;
        }
    } 
    

    

 })
}else {
    const get = fetch(`https://prince-cs2.herokuapp.com/api/courses/`)


         get.then(res => res.json())
         .then(data => {

            let courses;
            for(let i = 0; i< data.length; i++){
                if(courses){
                    welcome.innerHTML = "Welcome to Coding Verse!"
                    navitem.innerHTML = 
                `
                    <li class="nav-item">
        				<a class="nav-link text-light" href="./profile.html">Profile</a>
        			</li>
                
                `
                    courses = `${courses}
                    <div class="col-md-4 my-3">
                        <div class="card">
                            
                            <div class="card-body">
                                <h2 class="card-title specificname"> ${data[i].name}</h2>
                                <p class="card-title"> ${data[i].description}</p>
                                <p class="card-text">Price: ${data[i].price}</p>
                            </div>
                            <div class="card-footer" id="userButton">
                                <a onclick="enroll()"  class="btn btn-light bg-dark text-light">
                                    Enroll Now
                                </a>
                            </div>
                        </div>
                    </div>`
                }else{
                    courses = `<div class="col-md-4 my-3">
                        <div class="card">
                            
                            <div class="card-body">
                                <h2 class="card-title specificname"> ${data[i].name}</h2>
                                <p class="card-title"> ${data[i].description}</p>
                                <p class="card-text">Price: ${data[i].price}</p>
                            </div>
                            <div class="card-footer" id="userButton">
                                <a onclick="enroll()" class="btn btn-light bg-dark text-light">
                                    Enroll Now
                                </a>
                            </div>
                        </div>
                    </div>`
                }
                if(courses){
                    document.querySelector("#courseContainer").innerHTML = courses;
                }
            } 
    
})

}
})
}


function logout(){
	localStorage.clear();
	window.location.replace('./logout.html');
}
